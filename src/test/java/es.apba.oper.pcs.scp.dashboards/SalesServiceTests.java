package es.apba.oper.pcs.scp.dashboards;

import es.apba.oper.pcs.scp.dashboards.Services.SalesServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.TestPropertySource;

@SuppressWarnings("rawtypes")
@RunWith(MockitoJUnitRunner.class)
@TestPropertySource("classpath:application-test.properties")
public class SalesServiceTests
{
    @InjectMocks
    SalesServiceImpl salesService = new SalesServiceImpl();

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);
        salesService.postConstruct();
    }

    @Test
    public void test1()
    {
        salesService.requestForData(1);

    }
}