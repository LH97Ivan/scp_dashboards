function dateFormat(keys){
    var dates = keys.replace("[", "").replace("]", "").split(", ");

    for (var i = 0; i < dates.length; i++)
    {
        dates[i] = dates[i].substring(6,8) + "/" + dates[i].substring(4,6);
    }

    return dates;
}

function createColumnsPassengersVehicles(pieName, pieDescription, passengersValues, vehiclesValues, actualDay, keys, threshold1, threshold2, isMonochrome){

    var dates = dateFormat(keys);
    var actual = actualDay.toString().substring(6,8) + "/" + actualDay.toString().substring(4,6);
    var options = {
          series: [{
          name: 'Pasajeros',
          data: passengersValues
        }, {
          name: 'Vehículos',
          data: vehiclesValues
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        annotations: {
            xaxis: [{
                x: actual,
                strokeDashArray: 0,
                borderColor: '#ff0000',
                label: {
                    orientation: 'horizontal',
                    borderColor: '#ff0000',
                    style: {
                        color: '#fff',
                        background: '#ff0000',
                    }
                }
            }],
            yaxis: [{
                y: threshold1,
                borderColor: '#F0F713',
                label: {
                    borderColor: '#F0F713',
                    style: {
                        color: '#000000',
                        background: '#F0F713',
                    },
                    text: threshold1,
                }
            },{
                y: threshold2,
                borderColor: '#F50505',
                label: {
                    borderColor: '#F50505',
                    style: {
                        color: '#000000',
                        background: '#F50505',
                    },
                    text: threshold2,
                }
            }]
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: dates,
        },
        yaxis: {
          title: {
            text: 'billetes'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "" + val + " billetes"
            }
          }
        }
    };

    var chart = new ApexCharts(document.querySelector("#" + pieName), options);
    chart.render();
};

function createColumnsBuses(pieName, pieDescription, values, keys, actualDay, threshold1, threshold2, isMonochrome){

    var dates = dateFormat(keys);
    var actual = actualDay.toString().substring(6,8) + "/" + actualDay.toString().substring(4,6);
    var options = {
          series: [{
          name: 'Autobuses',
          data: values
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        annotations: {
            xaxis: [{
                x: actual,
                strokeDashArray: 0,
                borderColor: '#ff0000',
                label: {
                    orientation: 'horizontal',
                    borderColor: '#ff0000',
                    style: {
                        color: '#fff',
                        background: '#ff0000',
                    }
                }
            }],
            yaxis: [{
                y: threshold1,
                borderColor: '#F0F713',
                label: {
                    borderColor: '#F0F713',
                    style: {
                        color: '#000000',
                        background: '#F0F713',
                    },
                    text: threshold1,
                }
            },{
                y: threshold2,
                borderColor: '#F50505',
                label: {
                    borderColor: '#F50505',
                    style: {
                        color: '#000000',
                        background: '#F50505',
                    },
                    text: threshold2,
                }
            }]
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: dates,
        },
        yaxis: {
          title: {
            text: 'billetes'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "" + val + " billetes"
            }
          }
        }
    };

    var chart = new ApexCharts(document.querySelector("#" + pieName), options);
    chart.render();
};