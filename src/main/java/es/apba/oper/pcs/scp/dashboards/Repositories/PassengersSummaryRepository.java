package es.apba.oper.pcs.scp.dashboards.Repositories;

import es.apba.oper.pcs.scp.dashboards.Data.PassengersSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PassengersSummaryRepository extends JpaRepository<PassengersSummary, Long>
{

    List<PassengersSummary> findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(Date startTime, Date endTime);

}
