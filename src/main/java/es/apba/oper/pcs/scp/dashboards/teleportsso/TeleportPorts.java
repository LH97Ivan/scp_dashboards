package es.apba.oper.pcs.scp.dashboards.teleportsso;

import lombok.Data;

@Data
public class TeleportPorts
{
    String nombre;
    String locode;
}
