package es.apba.oper.pcs.scp.dashboards.Beans;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

@Data
public class IssuingCompanyGraphicDataBean implements Serializable {

    private Map<String, Integer> issuingCompanies;

    public IssuingCompanyGraphicDataBean(){
        this.issuingCompanies = new TreeMap<>();
    }

    public IssuingCompanyGraphicDataBean(Map<String, Integer> issuingCompanies) {
        this.issuingCompanies = issuingCompanies;
    }
}
