package es.apba.oper.pcs.scp.dashboards.Services;

import es.apba.oper.pcs.scp.dashboards.Data.UserData;
import es.apba.oper.pcs.scp.dashboards.teleportsso.UserDataDTOWS;
import org.springframework.stereotype.Service;

@Service
public class UserDataService
{
    public UserData getUserDataFromTeleportResponse(UserDataDTOWS userNameByTeleport)
    {
        UserData userData = null;
        boolean hasPermit = userNameByTeleport.getFuncs().contains("AMS");
        if (hasPermit)
        {
            boolean isAdmin = userNameByTeleport.getFuncs().contains("FCS");
            userData = new UserData();
            userData.setUsername(userNameByTeleport.getUser());
            userData.setAdmin(isAdmin);
            userData.setCompanyName(isAdmin ? userNameByTeleport.getCodusuberman() : "AML");
        }
        return userData;
    }

    public UserData createSimpleUser(String userName)
    {
        UserData userData = new UserData();
        userData.setUsername(userName);
        userData.setAdmin(userName.equalsIgnoreCase("APBA") || userName.equalsIgnoreCase("ADDOCEAN"));
        userData.setCompanyName(userName);
        return userData;
    }
}
