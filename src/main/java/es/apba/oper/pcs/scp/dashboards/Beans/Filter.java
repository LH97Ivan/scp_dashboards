package es.apba.oper.pcs.scp.dashboards.Beans;

import org.springframework.stereotype.Component;

import javax.faces.view.ViewScoped;
import java.io.Serializable;

@Component
@ViewScoped
public class Filter implements Serializable {

    private boolean prueba;
    private int total;

    public boolean isPrueba() {
        return prueba;
    }

    public void setPrueba(boolean prueba) {
        this.prueba = prueba;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void filtering(){
        if(prueba){
            setTotal(0);
        } else {
            setTotal(100);
        }
    }
}
