package es.apba.oper.pcs.scp.dashboards.Beans;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

@Data
public class VehiclesTicketsDataBean implements Serializable {

    private Map<String, Integer> vehiclesType;

    public VehiclesTicketsDataBean(){
        vehiclesType = new TreeMap<>();
    }

    public VehiclesTicketsDataBean(Map<String, Integer> vehiclesType) {
        this.vehiclesType = vehiclesType;
    }
}
