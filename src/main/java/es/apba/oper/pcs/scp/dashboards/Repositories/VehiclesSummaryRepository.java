package es.apba.oper.pcs.scp.dashboards.Repositories;

import es.apba.oper.pcs.scp.dashboards.Data.VehiclesSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface VehiclesSummaryRepository extends JpaRepository<VehiclesSummary, Long> {

    List<VehiclesSummary> findByDepartureTimeGreaterThanAndDepartureTimeLessThan(Date startTime, Date endTime);
}
