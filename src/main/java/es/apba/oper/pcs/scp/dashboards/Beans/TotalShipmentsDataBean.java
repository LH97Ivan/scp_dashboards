package es.apba.oper.pcs.scp.dashboards.Beans;

import lombok.Data;

import java.io.Serializable;

@Data
public class TotalShipmentsDataBean implements Serializable
{
    private long closedNumber;
    private long openedNumber;
    private long notReportNumber;
    private long newTickets;
    private long downTickets;
    private long pendingTickets;
}
