package es.apba.oper.pcs.scp.dashboards.Beans;

import es.apba.oper.pcs.scp.dashboards.Data.UserData;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import javax.annotation.ManagedBean;
import java.io.Serializable;

@ManagedBean
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Getter
@Setter
public class LoginBean
{
	private UserData user;

	public String getUserHeader()
	{
		return user.getUsername() + "(" + user.getCompanyName() + ")" + (user.isAdmin() ? ". ADMIN" : ".");
	}
}
