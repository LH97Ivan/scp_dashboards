package es.apba.oper.pcs.scp.dashboards.Views;

import es.apba.oper.pcs.scp.dashboards.Beans.*;
import es.apba.oper.pcs.scp.dashboards.Data.SalesOpenData;
import es.apba.oper.pcs.scp.dashboards.Data.UserData;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;

@Data
@Named
@ViewScoped
public class SalesViewBean implements Serializable
{
    private Date startTime = Date.from(Instant.now());

    private Map<String, Boolean> companyFilter = new HashMap<>();
    private Map<String, Boolean> arrivalPortFilter = new HashMap<>();
    private Boolean allCompanyFilters;

    private SalesDataBean salesDataBean;
    private SalesOpenData salesOpenData;

    private final List<String> totalCompanies = Arrays.asList("AML", "BALEARIA", "FRS", "INTERSHIPPING", "TRASMEDITERRANEA", "ADDOCEAN_1", "ADDOCEAN_2", "ADDOCEAN_3");
    private final List<String> totalArrivalPorts = Arrays.asList("ESCEU", "MAPTM", "MATNG");

    @Autowired
    private SalesViewPresenter salesViewPresenter;

    @Autowired
    private LoginBean loginBean;

    private static final Logger log = LoggerFactory.getLogger(SalesViewBean.class);

    @PostConstruct
    public void init(){
        totalCompanies.forEach(company -> companyFilter.put(company, true));
        totalArrivalPorts.forEach(arrivalPort -> arrivalPortFilter.put(arrivalPort, true));
        allCompanyFilters = false;
        initiateFilters(loginBean.getUser());
        onTotal();
    }

    public void initiateFilters(UserData user){
        totalCompanies.forEach(company -> companyFilter.put(company, false));
        if(user.isAdmin()){
            totalCompanies.forEach(company -> companyFilter.put(company, true));
        } else {
            companyFilter.put(user.getCompanyName().toUpperCase(), true);
        }
    }

    public void onTotal(){
        log.info("onTotal");

        salesDataBean = salesViewPresenter.getSalesData(companyFilter, arrivalPortFilter, startTime);
        salesOpenData = salesViewPresenter.getSalesOpenData(companyFilter, arrivalPortFilter);
    }

    public void tableCompanyFilter(){
        log.info("tableCompanyFilter {} - {}", allCompanyFilters, companyFilter);

        onTotal();
    }

    public void activateCompaniesFilters(){
        log.info("activateCompaniesFilters {} - {}", allCompanyFilters, companyFilter);
        totalCompanies.forEach(company -> companyFilter.put(company, allCompanyFilters));

        onTotal();
    }
}
