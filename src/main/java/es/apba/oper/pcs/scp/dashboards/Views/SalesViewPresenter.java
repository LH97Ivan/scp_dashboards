package es.apba.oper.pcs.scp.dashboards.Views;

import es.apba.oper.pcs.scp.dashboards.Beans.SalesBoardingDTO;
import es.apba.oper.pcs.scp.dashboards.Beans.SalesDataBean;
import es.apba.oper.pcs.scp.dashboards.Data.SalesData;
import es.apba.oper.pcs.scp.dashboards.Data.SalesOpenData;
import es.apba.oper.pcs.scp.dashboards.Services.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class SalesViewPresenter
{
    @Autowired
    SalesService salesService;

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    public SalesDataBean getSalesData(Map<String, Boolean> companyFilter, Map<String, Boolean> arrivalPortFilter , Date timeToFindData)
    {
        SalesDataBean salesDataBean = new SalesDataBean();
        List<SalesBoardingDTO> boardingDTOs = new ArrayList<>();
        LocalDateTime dateOfFilter = timeToFindData.toInstant().atZone(ZoneId.of("Europe/Madrid")).toLocalDateTime();

        for (int i = 0; i < 10; i++)
        {
            String formattedDay = timeToFindData.toInstant().atZone(ZoneId.of("Europe/Madrid")).plusDays((long) i - 3).format(dateTimeFormatter);

            int totalBus = 0;
            int totalPassengers = 0;
            int totalVehicles = 0;

            for (Map.Entry<String, Boolean> entry : companyFilter.entrySet())
            {
                // Si no está en el filtro de visibles
                if (!Boolean.TRUE.equals(entry.getValue()))
                {
                    continue;
                }

                String company = entry.getKey();
                List<SalesData> datasFromCompany = salesService.getSalesData(company, formattedDay);
                for (SalesData salesData : datasFromCompany)
                {
                    for (Map.Entry<String, Boolean> arrivalPort : arrivalPortFilter.entrySet()){
                        // Si no está en el filtro de visibles
                        if (!Boolean.TRUE.equals(arrivalPort.getValue()))
                        {
                            continue;
                        }
                        if (salesData.getArrivalPort().equals(arrivalPort.getKey())){
                            totalBus += salesData.getTotalBus();
                            totalPassengers += salesData.getTotalPassengers();
                            totalVehicles += salesData.getTotalOtherVehicles();

                            if (salesData.getDepartureDate().getDayOfYear()==dateOfFilter.getDayOfYear() && notAllZeroValues(salesData.getTotalBus(), salesData.getTotalPassengers(), salesData.getTotalOtherVehicles()))
                            {
                                boardingDTOs.add(new SalesBoardingDTO(company, salesData));
                            }
                            break;
                        }
                    }

                }
            }

            boardingDTOs.sort(Comparator.comparing(SalesBoardingDTO::getDepartureTime));

            salesDataBean.getBusesData().add(totalBus);
            salesDataBean.getPassengersData().add(totalPassengers);
            salesDataBean.getVehiclesData().add(totalVehicles);
            salesDataBean.getDaysApplied().add(formattedDay);
            salesDataBean.setBoardings(boardingDTOs);
        }

        return salesDataBean;
    }

    public boolean notAllZeroValues(Integer buses, Integer passengers, Integer vehicles)
    {
        Boolean res = false;
        if(buses != 0 || passengers != 0 || vehicles != 0){
            res = true;
        }
        return res;
    }

    public SalesOpenData getSalesOpenData(Map<String, Boolean> companyFilter, Map<String, Boolean> arrivalPortFilter)
    {
        SalesOpenData salesOpenData = new SalesOpenData();
        List<SalesOpenData> salesOpenDataFromCompanyAndPort = new ArrayList<>();
        List<SalesOpenData> saleOpenDataFromCompanyAndPortOrderByEffectiveDate;
        int totalBus = 0;
        int totalPassengers = 0;
        int totalVehicles = 0;

        for (Map.Entry<String, Boolean> entry : companyFilter.entrySet())
        {
            // Si no está en el filtro de visibles
            if (!Boolean.TRUE.equals(entry.getValue()))
            {
                continue;
            }

            String company = entry.getKey();

            for (Map.Entry<String, Boolean> arrivalPort : arrivalPortFilter.entrySet())
            {
                // Si no está en el filtro de visibles
                if (!Boolean.TRUE.equals(arrivalPort.getValue()))
                {
                    continue;
                }

                saleOpenDataFromCompanyAndPortOrderByEffectiveDate = salesService.findTopSalesOpenDataFromCompanyAndPortOrderByEffectiveDateDesc(company, arrivalPort.getKey());
                salesOpenDataFromCompanyAndPort.addAll(saleOpenDataFromCompanyAndPortOrderByEffectiveDate);
            }
        }

        for (SalesOpenData sales : salesOpenDataFromCompanyAndPort)
        {
            totalBus += sales.getTotalBus();
            totalPassengers += sales.getTotalPassengers();
            totalVehicles += sales.getTotalOtherVehicles();
        }

        salesOpenData.setTotalBus(salesOpenData.getTotalBus() + totalBus);
        salesOpenData.setTotalPassengers(salesOpenData.getTotalPassengers() + totalPassengers);
        salesOpenData.setTotalOtherVehicles(salesOpenData.getTotalOtherVehicles() + totalVehicles);
        return salesOpenData;
    }
}
