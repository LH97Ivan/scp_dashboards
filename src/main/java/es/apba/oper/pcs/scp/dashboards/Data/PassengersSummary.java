package es.apba.oper.pcs.scp.dashboards.Data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class PassengersSummary implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer passengersNumber;
    private String type;
    private String issuingCompany;
    private String checkingCompany;
    private String vesselName;
    private Date departureTime;

    public PassengersSummary(){}

    public PassengersSummary(Long id, Integer passengersNumber, String type, String issuingCompany, String checkingCompany, String vesselName, Date departureTime) {
        this.id = id;
        this.passengersNumber = passengersNumber;
        this.type = type;
        this.issuingCompany = issuingCompany;
        this.checkingCompany = checkingCompany;
        this.vesselName = vesselName;
        this.departureTime = departureTime;
    }
}
