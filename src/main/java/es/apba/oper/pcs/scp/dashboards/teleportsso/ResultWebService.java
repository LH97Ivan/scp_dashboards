package es.apba.oper.pcs.scp.dashboards.teleportsso;

import lombok.Data;

@Data
public class ResultWebService
{

    private String resultado;
    private String codError;
    private String descError;
    private String descErrorNativo;

    public String getResultado() {
        return resultado;
    }
}
