package es.apba.oper.pcs.scp.dashboards.configuration;

import es.apba.oper.pcs.scp.dashboards.Beans.LoginBean;
import es.apba.oper.pcs.scp.dashboards.Data.UserData;
import es.apba.oper.pcs.scp.dashboards.Services.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

@Service
public class CustomUserDetailsService implements UserDetailsService
{
	@Autowired
	private LoginBean loginBean;

	private HashMap<String, String> users = new HashMap<>();

	@Autowired
	private UserDataService userDataService;

	private static final String USER_PASSWORD = "$2a$10$Df47ro4uttNjzCJrrqJMpuTdWBCyAokvPqZxGECofMau6Vz5iQhQW";

	@PostConstruct
	public void postConstruct()
	{
		users.put("addocean", USER_PASSWORD);
		users.put("apba", "$2a$10$X8YcVMHm9oMTMbqnhEz0u.C9xemrMG3x7nlMjxh4N3fxGVtVOyg9e");
		users.put("aml", USER_PASSWORD);
		users.put("balearia", USER_PASSWORD);
		users.put("frs", USER_PASSWORD);
		users.put("intershipping", USER_PASSWORD);
		users.put("trasmediterranea", USER_PASSWORD);
	}

	@Override
	public UserDetails loadUserByUsername(String userName) {

		UserDetails userDetails;

		String password = userName != null ? users.getOrDefault(userName, "") : "";
		if (!password.isEmpty())
		{
			userDetails = new User(userName, password, getAuthorities(userName));
			UserData userData = userDataService.createSimpleUser(userName);
			loginBean.setUser(userData);
		}
		else {
			throw new UsernameNotFoundException("UserData " + userName + " not found");
		}

		return userDetails;
	}

	private Collection<? extends GrantedAuthority> getAuthorities(String user) {
		return AuthorityUtils.createAuthorityList(getRolesForUser(user));
	}

	public String[] getRolesForUser(String user) {
		if (user.equals("addocean") || user.equals("apba")){
			return Arrays.asList("ROLE_USER", "ROLE_ADMIN").toArray(new String[0]);
		} else {
			return Arrays.asList("ROLE_USER").toArray(new String[0]);
		}
	}
}
