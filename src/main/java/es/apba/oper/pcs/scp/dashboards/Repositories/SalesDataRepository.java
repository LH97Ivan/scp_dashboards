package es.apba.oper.pcs.scp.dashboards.Repositories;

import es.apba.oper.pcs.scp.dashboards.Data.SalesData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface SalesDataRepository extends JpaRepository<SalesData, Long>
{
    Optional<SalesData> findByCompanyAndDepartureDate(String company, LocalDateTime from);

    List<SalesData> findByCompanyAndDepartureDateBetween(String company, LocalDateTime date1, LocalDateTime date2);
}
