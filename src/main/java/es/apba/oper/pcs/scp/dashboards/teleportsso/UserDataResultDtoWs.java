package es.apba.oper.pcs.scp.dashboards.teleportsso;

import lombok.Data;

@Data
public class UserDataResultDtoWs {

    private UserDataDTOWS[] userDataDTOWS;
    private ResultWebService resultWebService;

    public UserDataDTOWS[] getUserDataDTOWS() {
        return userDataDTOWS;
    }

    public ResultWebService getResultWebService() {
        return resultWebService;
    }
}
