package es.apba.oper.pcs.scp.dashboards.configuration;

import es.apba.oper.pcs.scp.dashboards.Services.SalesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Configuration
@Component
@EnableScheduling
public class DashboardDaemon
{
    private static final Logger log = LoggerFactory.getLogger(DashboardDaemon.class);

    @Autowired
    private SalesService salesService;

    @Scheduled(cron = "${scp.dashboard.cron.scheduled.sales-daemon}")
    public void salesDaemon()
    {
        log.info("Ejecutando el demonio de obtención de ventas");

        salesService.requestForData(7);

        log.info("Ejecutando el demonio de obtención de ventas. Fin.");
    }

    @Scheduled(cron = "${scp.dashboard.cron.scheduled.sales-daemon.long-term}")
    public void salesDaemonLongTerm()
    {
        log.info("Ejecutando el demonio de obtención de ventas. LongTerm.");

        salesService.requestForData(30);

        salesService.requestForOpenData();

        log.info("Ejecutando el demonio de obtención de ventas. LongTerm. Fin.");
    }

}
