package es.apba.oper.pcs.scp.dashboards.Services;

import es.apba.oper.pcs.scp.dashboards.Data.SalesData;
import es.apba.oper.pcs.scp.dashboards.Data.SalesOpenData;
import java.util.List;

public interface SalesService
{
    void requestForData(long longTerm);

    List<SalesData> getSalesData(String company, String date);

    void requestForOpenData();

    List<SalesOpenData> findTopSalesOpenDataFromCompanyAndPortOrderByEffectiveDateDesc(String company, String arrivalPort);
}
