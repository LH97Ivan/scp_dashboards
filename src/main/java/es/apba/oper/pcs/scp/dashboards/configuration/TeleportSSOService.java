package es.apba.oper.pcs.scp.dashboards.configuration;

import es.apba.oper.pcs.scp.dashboards.Data.UserData;
import es.apba.oper.pcs.scp.dashboards.Services.UserDataService;
import es.apba.oper.pcs.scp.dashboards.teleportsso.UserDataDTOWS;
import es.apba.oper.pcs.scp.dashboards.teleportsso.UserDataResultDtoWs;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Service
public class TeleportSSOService implements ITeleportSSOService
{
    private static final String PAUTH = "passwordauth";
	private static final String UAUTH = "usernameauth";

	@Value("${scp.teleportsso.urlCheckTeleportSession}")
    private String urlCheckTeleportSession;

    @Value("${scp.teleportsso.urlLogin}")
    private String urlLogin;

    @Value("${scp.teleportsso.urlCompanies}")
    private String urlCompanies;

    @Value("${scp.teleportsso.codapli}")
    private String codapli;

    @Value("${scp.teleportsso.usernameauth}")
    private String usernameauth;

    @Value("${scp.teleportsso.passwordauth}")
    private String passwordauth;

    @Value("${scp.teleportsso.agent}")
    private String agent;

    @Value("${scp.teleportsso.localname}")
    private String localname;

    @Value("${scp.teleportsso.client}")
    private String client;

    @Value("${scp.teleportsso.version}")
    private String version;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserDataService userDataService;

    private static final Logger logger = LoggerFactory.getLogger(TeleportSSOService.class);

    @Override
    public UserData doLoginWithTeleportSSOUsingSessionId(String sessionId)
    {
        UserData userData = null;

        if (!sessionId.isEmpty())
        {
            UserDataDTOWS userNameByTeleport = checkTeleportSession(sessionId);
            if (userNameByTeleport != null)
            {
                userData = userDataService.getUserDataFromTeleportResponse(userNameByTeleport);
                if (userData == null)
                {
                    logger.error("doLoginWithTeleportSSO Usuario {} con loggin erróneo", userNameByTeleport);
                }
            }
            else
            {
                logger.warn("doLoginWithTeleportSSO Not username found for session {}", sessionId);
            }
        }
        return userData;
    }

    @Override
    public UserData doLoginWithTeleportSSOUsingUsername(String username, String password)
    {
        UserData userData = null;

        if (!username.isEmpty())
        {
            UserDataDTOWS userNameByTeleport = checkTeleportUser(username, password);
            if (userNameByTeleport != null)
            {
                userData = userDataService.getUserDataFromTeleportResponse(userNameByTeleport);
                if (userData == null)
                {
                    logger.error("doLoginWithTeleportSSOUsingUsername Usuario {} con loggin erróneo", userNameByTeleport);
                }
            }
            else
            {
                logger.warn("doLoginWithTeleportSSOUsingUsername Not username logged {}", username);
            }
        }
        return userData;
    }

    private UserDataDTOWS checkTeleportUser(String username, String password)
    {
        UserDataDTOWS userNameByTeleport = null;

        logger.debug("checkTeleportUser {}", username);

        if (!urlLogin.trim().isEmpty())
        {
            logger.debug("valid sso url");

            final WebTarget webTarget = ClientBuilder.newClient().target(urlLogin);

            WebTarget webTarget1 = webTarget.queryParam(UAUTH, usernameauth)
                    .queryParam(PAUTH, passwordauth)
                    .queryParam("codapli", codapli)
                    .queryParam("usr", username)
                    .queryParam("password", password)
                    .queryParam("agent", agent)
                    .queryParam("localname", localname)
                    .queryParam("client", client)
                    .queryParam("version", version);
            URI uri = webTarget1.getUri();
            logger.debug("URI to request {}", uri);

            Response respuestaSSOTeleport = webTarget1.request().accept(MediaType.APPLICATION_JSON).get();

            logger.debug("Result = {}", respuestaSSOTeleport.getStatus());

            userNameByTeleport = processTeleportResponse(respuestaSSOTeleport);
        }
        return userNameByTeleport;
    }

    private UserDataDTOWS checkTeleportSession(String sessionId)
    {
        UserDataDTOWS userNameByTeleport = null;

        logger.debug("checkTeleportSession {}", sessionId);

        if (!urlCheckTeleportSession.trim().isEmpty())
        {
            logger.debug("valid sso url");

            final WebTarget webTarget = ClientBuilder.newClient().target(urlCheckTeleportSession);

            WebTarget webTarget1 = webTarget.queryParam(UAUTH, usernameauth)
                    .queryParam(PAUTH, passwordauth)
                    .queryParam("codapli", codapli)
                    .queryParam("publickey", sessionId);
            URI uri = webTarget1.getUri();
            logger.debug("URI to request {}", uri);

            Response respuestaSSOTeleport = webTarget1.request().accept(MediaType.APPLICATION_JSON).get();

            logger.debug("Result = {}", respuestaSSOTeleport.getStatus());

            userNameByTeleport = processTeleportResponse(respuestaSSOTeleport);
        }
        return userNameByTeleport;
    }

    private UserDataDTOWS processTeleportResponse(Response respuestaSSOTeleport)
    {
        UserDataDTOWS userdata = null;
        logger.debug("Respuesta desde Teleport {}", respuestaSSOTeleport);
        if (respuestaSSOTeleport.getStatusInfo().equals(Response.Status.OK))
        {
            UserDataResultDtoWs resultadoLlamada = respuestaSSOTeleport.readEntity(UserDataResultDtoWs.class);
            logger.debug("dataResult = {}", resultadoLlamada);
            logger.debug("getUserDataDTOWS = {}", (resultadoLlamada.getUserDataDTOWS() != null ? "!= null" : "null"));

            if (resultadoLlamada.getUserDataDTOWS() != null)
            {
                logger.debug("getUserDataDTOWS = {}", resultadoLlamada.getUserDataDTOWS().length);

                if (resultadoLlamada.getUserDataDTOWS().length > 0)
                {
                    userdata = resultadoLlamada.getUserDataDTOWS()[0];
                }
                else
                {
                    logger.warn("getUserDataDTOWS Result size = 0");
                }

                logger.debug("UserData = {}", userdata);
            }
        }
        return userdata;
    }
}
