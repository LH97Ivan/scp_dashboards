package es.apba.oper.pcs.scp.dashboards.exceptions;

import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import java.util.Iterator;

public class MyExceptionHandler extends ExceptionHandlerWrapper
{
    private ExceptionHandler wrapped;

    Logger log;
    FacesContext fc;

    public MyExceptionHandler(ExceptionHandler exceptionHandler)
    {
        wrapped = exceptionHandler;
        log = Logger.getLogger(this.getClass().getName());
        fc = FacesContext.getCurrentInstance();
    }

    @Override
    public ExceptionHandler getWrapped()
    {
        return wrapped;
    }

    @Override
    public void handle()
    {
        Iterator<ExceptionQueuedEvent> i = super.getUnhandledExceptionQueuedEvents().iterator();

        while (i.hasNext())
        {

            Throwable t = i.next().getContext().getException();
            log.warn("Exception handlede " + t.toString());

            if (t instanceof ViewExpiredException)
            {
                try
                {
                    // Handle the exception, for example:
                    log.error("ViewExpiredException occurred!");
                    fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "An exception occurred",
                            "ViewExpiredException happened!"));
                    fc.getApplication().getNavigationHandler().handleNavigation(fc, null, "ViewExpiredException");
                }
                finally
                {
                    i.remove();
                }
            }
            else if (t instanceof FacesException)
            {
                PrimeFaces current = PrimeFaces.current();
                current.executeScript("PF('exceptionOcurred').show();");
            }

        }

        // let the parent handle the rest
        getWrapped().handle();
    }
}
