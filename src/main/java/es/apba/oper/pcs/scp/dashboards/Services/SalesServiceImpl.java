package es.apba.oper.pcs.scp.dashboards.Services;

import es.apba.oper.pcs.scp.dashboards.Data.SalesData;
import es.apba.oper.pcs.scp.dashboards.Data.SalesOpenData;
import es.apba.oper.pcs.scp.dashboards.Data.rest.SalesREST;
import es.apba.oper.pcs.scp.dashboards.Repositories.SalesDataRepository;
import es.apba.oper.pcs.scp.dashboards.Repositories.SalesOpenDataRepository;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@Service
public class SalesServiceImpl implements SalesService
{
    private static final String BALEARIA = "BALEARIA";
    private static final String AML = "AML";
    private static final String FRS = "FRS";
    private static final String INTERSHIPPING = "INTERSHIPPING";
    private static final String TRASMEDITERRANEA = "TRASMEDITERRANEA";
    private static final String ADDOCEAN_1 = "ADDOCEAN_1";
    private static final String ADDOCEAN_2 = "ADDOCEAN_2";
    private static final String ADDOCEAN_3 = "ADDOCEAN_3";

    private final List<String> companiesToRequestSalesData = Arrays.asList(TRASMEDITERRANEA, AML, BALEARIA, FRS, INTERSHIPPING, ADDOCEAN_1, ADDOCEAN_2, ADDOCEAN_3);

    @Autowired
    private SalesDataRepository salesDataRepository;

    @Autowired
    private SalesOpenDataRepository salesOpenDataRepository;

    @Value("${scp.dashboard.url." + AML + "}")
    private String amlUrl;

    @Value("${scp.dashboard.token." + AML + "}")
    private String amlToken;

    @Value("${scp.dashboard.url." + BALEARIA + "}")
    private String baleariaUrl;

    @Value("${scp.dashboard.token." + BALEARIA + "}")
    private String baleariaToken;

    @Value("${scp.dashboard.url." + FRS + "}")
    private String frsUrl;

    @Value("${scp.dashboard.token." + FRS + "}")
    private String frsToken;

    @Value("${scp.dashboard.url." + INTERSHIPPING + "}")
    private String intershippingUrl;

    @Value("${scp.dashboard.token." + INTERSHIPPING + "}")
    private String intershippingToken;

    @Value("${scp.dashboard.url." + TRASMEDITERRANEA +"}")
    private String trasmediterraneaUrl;

    @Value("${scp.dashboard.token." + TRASMEDITERRANEA + "}")
    private String trasmediterraneaToken;

    @Value("${scp.dashboard.url." + ADDOCEAN_1 + "}")
    private String addocean1Url;

    @Value("${scp.dashboard.token." + ADDOCEAN_1 + "}")
    private String addocean1Token;

    @Value("${scp.dashboard.url." + ADDOCEAN_2 + "}")
    private String addocean2Url;

    @Value("${scp.dashboard.token." + ADDOCEAN_2 + "}")
    private String addocean2Token;

    @Value("${scp.dashboard.url." + ADDOCEAN_3 + "}")
    private String addocean3Url;

    @Value("${scp.dashboard.token." + ADDOCEAN_3 + "}")
    private String addocean3Token;

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    DateTimeFormatter dateTimeFormatterWithHour = DateTimeFormatter.ofPattern("yyyyMMdd HHmm");

    private static final String HEADER_ACCEPT = "Accept";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String CONTENT_TYPE = "Content-Type";

    private final List<String> ports = Arrays.asList("ESCEU", "MAPTM", "MATNG");
    private final Map<String, String> urls = new HashMap<>();
    private final Map<String, String> tokens = new HashMap<>();

    private static final Logger log = LoggerFactory.getLogger(SalesServiceImpl.class);

    @PostConstruct
    public void postConstruct()
    {
        urls.put(AML, amlUrl);
        urls.put(BALEARIA, baleariaUrl);
        urls.put(FRS, frsUrl);
        urls.put(INTERSHIPPING, intershippingUrl);
        urls.put(TRASMEDITERRANEA, trasmediterraneaUrl);
        urls.put(ADDOCEAN_1, addocean1Url);
        urls.put(ADDOCEAN_2, addocean2Url);
        urls.put(ADDOCEAN_3, addocean3Url);
        tokens.put(AML, amlToken);
        tokens.put(BALEARIA, baleariaToken);
        tokens.put(FRS, frsToken);
        tokens.put(INTERSHIPPING, intershippingToken);
        tokens.put(TRASMEDITERRANEA, trasmediterraneaToken);
        tokens.put(ADDOCEAN_1, addocean1Token);
        tokens.put(ADDOCEAN_2, addocean2Token);
        tokens.put(ADDOCEAN_3, addocean3Token);

        // requestForOpenData();
        // requestForData(1);
    }

    @Override
    public void requestForData(long longTerm)
    {
        companiesToRequestSalesData.stream().filter(this::isValidCompany).forEach(company -> requestForDataToCompany(company, longTerm));
    }

    @Override
    public void requestForOpenData()
    {
        companiesToRequestSalesData.stream().filter(this::isValidCompany).forEach(this::requestForOpenDataToCompany);
    }

    @Override
    public List<SalesData> getSalesData(String company, String date)
    {
        LocalDateTime date1 = LocalDateTime.parse(date + " 0000", dateTimeFormatterWithHour);
        LocalDateTime date2 = LocalDateTime.parse(date + " 2359", dateTimeFormatterWithHour);
        return salesDataRepository.findByCompanyAndDepartureDateBetween(company, date1, date2);
    }

    private void requestForDataToCompany(String company, long daysToApply)
    {
        log.info("requestForDataToCompany {}", company);
        for (int i = 0; i < daysToApply; i++)
        {
            String formattedDay = LocalDate.now().plusDays(i).format(dateTimeFormatter);

            List<SalesREST> requestResult = new ArrayList<>();
            ports.forEach(port -> requestResult.addAll(requestForDataToCompanyAndDate(company, port, formattedDay)));
            requestResult.forEach(salesREST -> persistSalesFromSalesRest(company, salesREST));
        }
    }

    private void requestForOpenDataToCompany(String company)
    {
        log.info("requestForDataToCompany {}", company);

        List<SalesREST> requestResult = new ArrayList<>();
        ports.forEach(port -> requestResult.addAll(requestForDataToCompanyAndDate(company, port, null)));
        requestResult.forEach(salesREST -> persistOpenSalesFromSalesRest(company, salesREST));
    }

    private void persistSalesFromSalesRest(String company, SalesREST requestResult)
    {
        try
        {
            LocalDateTime departureDate = LocalDateTime.parse(requestResult.getDepartureDate() + " " + requestResult.getDepartureTime().substring(0, 4), dateTimeFormatterWithHour);

            SalesData salesData = salesDataRepository.findByCompanyAndDepartureDate(company, departureDate).orElse(new SalesData());
            salesData.setArrivalPort(requestResult.getArrivalPort());
            salesData.setCompany(company);
            salesData.setDepartureDate(departureDate);
            salesData.setTotalBus(requestResult.getTotalBus());
            salesData.setTotalPassengers(requestResult.getTotalPassengers());
            salesData.setTotalOtherVehicles(requestResult.getTotalOtherVehicles());
            salesData.setRequestedTime(Instant.now());

            salesDataRepository.save(salesData);
        }
        catch (DateTimeParseException dtpe)
        {
            log.error("Excepción formateando fecha recibida {}", dtpe.getMessage());
        }
    }

    private void persistOpenSalesFromSalesRest(String company, SalesREST requestResult)
    {
        LocalDate effectiveDate = Instant.now().atZone(ZoneId.of("Europe/Madrid")).toLocalDate();

        SalesOpenData salesOpenData = salesOpenDataRepository.findByCompanyAndEffectiveDate(company, effectiveDate).orElse(new SalesOpenData());
        salesOpenData.setArrivalPort(requestResult.getArrivalPort());
        salesOpenData.setCompany(company);
        salesOpenData.setEffectiveDate(effectiveDate);
        salesOpenData.setTotalBus(requestResult.getTotalBus());
        salesOpenData.setTotalPassengers(requestResult.getTotalPassengers());
        salesOpenData.setTotalOtherVehicles(requestResult.getTotalOtherVehicles());
        salesOpenData.setRequestedTime(Instant.now());

        salesOpenDataRepository.save(salesOpenData);
    }

    private List<SalesREST> requestForDataToCompanyAndDate(String company, String arrivalPort, String formattedDay)
    {
        List<SalesREST> result = new ArrayList<>();

        try
        {
            log.info("requestForDataToCompanyAndDate Company {}. Port = {}. Day = {}", company, arrivalPort, formattedDay);

            RestTemplate restTemplate = getRestTemplate();

            HttpHeaders header = new HttpHeaders();
            header.set(HEADER_ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            header.set(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            header.set(HEADER_AUTHORIZATION, "Basic " + getBearerForCompany(company));

            HttpEntity<?> entity = new HttpEntity<>(header);

            String urlCompleta = getURLForCompany(company) + "?arrivalPort=" + arrivalPort;
            if (formattedDay != null)
            {
                urlCompleta = urlCompleta + "&departureDate=" + formattedDay;
            }

            ResponseEntity<SalesREST[]> restResult = restTemplate.exchange(urlCompleta, HttpMethod.GET, entity, SalesREST[].class);
            result = restResult.getStatusCode().is2xxSuccessful() && restResult.getBody() != null ? Arrays.asList(restResult.getBody()) : result;

            log.info("requestForDataToCompanyAndDate Company {}. Port = {}. Day = {}. Result = {}", company, arrivalPort, formattedDay, result.size());

        }
        catch (RestClientException | IllegalArgumentException ex)
        {
            log.error(ex.getMessage());
        }

        return result;
    }

    private RestTemplate getRestTemplate()
    {
        RestTemplate restTemplate = new RestTemplate();

        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();

            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);
            requestFactory.setConnectTimeout(60000);
            requestFactory.setConnectionRequestTimeout(60000);
            requestFactory.setReadTimeout(60000);

            restTemplate = new RestTemplate(requestFactory);

        } catch (Exception ex) {
            log.info(String.valueOf(Arrays.asList(ex.getStackTrace())));
        }

        return restTemplate;
    }

    private String getBearerForCompany(String company)
    {
        return tokens.getOrDefault(company, "");
    }

    private String getURLForCompany(String company)
    {
        return urls.getOrDefault(company, "");
    }

    private boolean isValidCompany(String company)
    {
        String companyUrl = urls.getOrDefault(company, "");
        return companyUrl != null && !companyUrl.trim().isEmpty();
    }

    public List<SalesOpenData> findTopSalesOpenDataFromCompanyAndPortOrderByEffectiveDateDesc(String company, String arrivalPort)
    {
        return salesOpenDataRepository.findByCompanyAndArrivalPort(company, arrivalPort);
    }
}
