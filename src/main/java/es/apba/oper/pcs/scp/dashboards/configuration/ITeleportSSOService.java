package es.apba.oper.pcs.scp.dashboards.configuration;

import es.apba.oper.pcs.scp.dashboards.Data.UserData;

public interface ITeleportSSOService
{
    UserData doLoginWithTeleportSSOUsingSessionId(String sessionId);
    UserData doLoginWithTeleportSSOUsingUsername(String username, String password);
}
