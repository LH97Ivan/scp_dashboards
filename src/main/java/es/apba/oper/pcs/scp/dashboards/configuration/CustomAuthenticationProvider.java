package es.apba.oper.pcs.scp.dashboards.configuration;

import es.apba.oper.pcs.scp.dashboards.Beans.LoginBean;
import es.apba.oper.pcs.scp.dashboards.Data.UserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider
{
    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

    @Autowired
    private LoginBean loginBean;

    @Autowired
    private TeleportSSOService teleportSSOService;

    @Override
    public Authentication authenticate(Authentication authentication)
    {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        String passMD5 = getMD5(password);

        logger.info("authenticate {} {}", name, passMD5);

        Authentication authenticationResult = null;
        UserData userData = shouldAuthenticateAgainstThirdPartySystem(name, passMD5);
        if (userData != null)
        {
            loginBean.setUser(userData);

            // use the credentials
            // and authenticate against the third-party system
            authenticationResult = new UsernamePasswordAuthenticationToken(name, passMD5, getAuthorities(name));
        }

        return authenticationResult;
    }

    private UserData shouldAuthenticateAgainstThirdPartySystem(String username, String password)
    {
        return teleportSSOService.doLoginWithTeleportSSOUsingUsername(username, password);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private Collection<? extends GrantedAuthority> getAuthorities(String user) {
        return AuthorityUtils.createAuthorityList(getRolesForUser(user));
    }

    public String[] getRolesForUser(String user) {
        /*
        return user.getRoles().stream().map(Role::toString).collect(Collectors.toList()).
                toArray(new String[user.getRoles().size()]);
         */

        return Collections.singletonList("ROLE_USER").toArray(new String[1]);
    }

    public static String getMD5(String input) {
        StringBuilder hashtext = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            hashtext = new StringBuilder(number.toString(16));

            while (hashtext.length() < 32) {
                hashtext.insert(0, "0");
            }
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return hashtext.toString();
    }
}
