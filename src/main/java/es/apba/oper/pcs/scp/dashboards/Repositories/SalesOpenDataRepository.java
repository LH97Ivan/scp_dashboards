package es.apba.oper.pcs.scp.dashboards.Repositories;

import es.apba.oper.pcs.scp.dashboards.Data.SalesOpenData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SalesOpenDataRepository extends JpaRepository<SalesOpenData, Long>
{
    Optional<SalesOpenData> findByCompanyAndEffectiveDate(String company, LocalDate effectiveDate);

    List<SalesOpenData> findByCompanyAndArrivalPort(String company, String arrivalPort);

}
