package es.apba.oper.pcs.scp.dashboards.Data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

@Data
@Entity
public class SalesOpenData implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    String company;

    LocalDate effectiveDate;
    String arrivalPort;
    Integer totalPassengers;
    Integer totalBus;
    Integer totalOtherVehicles;

    Instant requestedTime;

    public SalesOpenData(){
        this.totalPassengers = 0;
        this.totalBus = 0;
        this.totalOtherVehicles = 0;
    }

}
