package es.apba.oper.pcs.scp.dashboards.teleportsso;

import lombok.Data;

import java.util.List;

@Data
public class UserDataDTOWS
{

    private String codautpor;
    private String subpue;
    private String codapli;
    private String desapli;
    private String urlapli;
    private String username;
    private int codorg;
    private String codusuberman;
    private String cif;
    private String nomconsignatario;
    private List<String> funcs;
    private List<String> tiposorg;
    private String token;
    private String user;
    private int esautpor;
    private String exentoisps;
    private String filtroescalas;
    private String email;
    private List<TeleportPorts> ports;
}
