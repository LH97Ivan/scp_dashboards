package es.apba.oper.pcs.scp.dashboards.Beans;

import es.apba.oper.pcs.scp.dashboards.Data.BoardingDataSummary;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Boarding implements Serializable {

    private String vessel;
    private Date departureTime;
    private String checkingCompany;
    private Integer newTickets;
    private Integer downTickets;
    private Integer pendingTickets;

    public Boarding(BoardingDataSummary boardingData) {
        vessel = boardingData.getVesselName();
        departureTime = boardingData.getDepartureTime();
        checkingCompany = boardingData.getCompany();
        newTickets = boardingData.getNewTickets();
        downTickets = boardingData.getDownTickets();
        pendingTickets = boardingData.getPendingTickets();
    }
}
