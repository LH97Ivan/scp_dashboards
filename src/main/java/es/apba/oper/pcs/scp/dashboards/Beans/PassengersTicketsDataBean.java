package es.apba.oper.pcs.scp.dashboards.Beans;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

@Data
public class PassengersTicketsDataBean implements Serializable {

    private Map<String, Integer> passengersType;

    public PassengersTicketsDataBean(){
        passengersType = new TreeMap<>();
    }

    public PassengersTicketsDataBean(Map<String, Integer> passengersType) {
        this.passengersType = passengersType;
    }
}
