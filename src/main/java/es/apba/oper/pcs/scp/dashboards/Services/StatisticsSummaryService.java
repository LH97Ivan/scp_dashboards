package es.apba.oper.pcs.scp.dashboards.Services;

import es.apba.oper.pcs.scp.dashboards.Beans.*;
import es.apba.oper.pcs.scp.dashboards.Data.BoardingDataSummary;
import es.apba.oper.pcs.scp.dashboards.Data.PassengersSummary;
import es.apba.oper.pcs.scp.dashboards.Data.VehiclesSummary;
import es.apba.oper.pcs.scp.dashboards.Repositories.BoardingDataRepository;
import es.apba.oper.pcs.scp.dashboards.Repositories.PassengersSummaryRepository;
import es.apba.oper.pcs.scp.dashboards.Repositories.VehiclesSummaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;

import java.util.*;

@Service
@ApplicationScope
public class StatisticsSummaryService {

    @Autowired
    PassengersSummaryRepository passengersSummaryRepository;

    @Autowired
    VehiclesSummaryRepository vehiclesSummaryRepository;

    @Autowired
    BoardingDataRepository boardingDataRepository;

    public int getTotalVehiclesAndPassengers(Date startTime, Date endTime) {
        int result = 0;
        List<PassengersSummary> passengers = passengersSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);
        List<VehiclesSummary> vehicles = vehiclesSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThan(startTime, endTime);
        for (PassengersSummary passengersSummary : passengers) {
            result += passengersSummary.getPassengersNumber();
        }
        for(VehiclesSummary vehiclesSummary : vehicles){
            result += vehiclesSummary.getVehiclesNumber();
        }
        return result;
    }

    public TotalTicketsDataBean getTotalTicketsDataBean(Date startTime, Date endTime, Map<String, Boolean> issuingCompaniesSelected, Map<String, Boolean> checkingCompaniesSelected){
        TotalTicketsDataBean totalTicketsDataBean = new TotalTicketsDataBean();
        List<PassengersSummary> passengers = passengersSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);
        String passengersKey = "Passengers";
        for(PassengersSummary passengersSummary: passengers){
            if(issuingCompaniesSelected.getOrDefault (passengersSummary.getIssuingCompany(), false) &&
                    checkingCompaniesSelected.getOrDefault(passengersSummary.getCheckingCompany(), false)){
                if(!totalTicketsDataBean.getTotalTickets().containsKey(passengersKey)){
                    totalTicketsDataBean.getTotalTickets().put(passengersKey, passengersSummary.getPassengersNumber());
                } else {
                    Integer actualValue = totalTicketsDataBean.getTotalTickets().get(passengersKey);
                    totalTicketsDataBean.getTotalTickets().put(passengersKey, actualValue+passengersSummary.getPassengersNumber());
                }
            }

        }
        List<VehiclesSummary> vehicles = vehiclesSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThan(startTime, endTime);
        String vehiclesKey = "Vehicles";
        for(VehiclesSummary vehiclesSummary: vehicles){
            if(issuingCompaniesSelected.getOrDefault(vehiclesSummary.getIssuingCompany(), false) &&
                    checkingCompaniesSelected.getOrDefault(vehiclesSummary.getCheckingCompany(), false)){
                if(!totalTicketsDataBean.getTotalTickets().containsKey(vehiclesKey)){
                    totalTicketsDataBean.getTotalTickets().put(vehiclesKey, vehiclesSummary.getVehiclesNumber());
                } else {
                    Integer actualValue = totalTicketsDataBean.getTotalTickets().get(vehiclesKey);
                    totalTicketsDataBean.getTotalTickets().put(vehiclesKey, actualValue+vehiclesSummary.getVehiclesNumber());
                }
            }
        }
        return totalTicketsDataBean;
    }

    public IssuingCompanyGraphicDataBean getIssuingCompanyGraphicDataBean(Date startTime, Date endTime){
        IssuingCompanyGraphicDataBean issuingCompanyGraphicDataBean = new IssuingCompanyGraphicDataBean();
        List<PassengersSummary> passengers = passengersSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);
        for(PassengersSummary passengersSummary: passengers){
            if(!issuingCompanyGraphicDataBean.getIssuingCompanies().containsKey(passengersSummary.getIssuingCompany())){
                issuingCompanyGraphicDataBean.getIssuingCompanies().put(passengersSummary.getIssuingCompany(), passengersSummary.getPassengersNumber());
            } else {
                Integer actualValue = issuingCompanyGraphicDataBean.getIssuingCompanies().get(passengersSummary.getIssuingCompany());
                issuingCompanyGraphicDataBean.getIssuingCompanies().put(passengersSummary.getIssuingCompany(), actualValue+passengersSummary.getPassengersNumber());
            }
        }
        List<VehiclesSummary> vehicles = vehiclesSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThan(startTime, endTime);
        for(VehiclesSummary vehiclesSummary: vehicles){
            if(!issuingCompanyGraphicDataBean.getIssuingCompanies().containsKey(vehiclesSummary.getIssuingCompany())){
                issuingCompanyGraphicDataBean.getIssuingCompanies().put(vehiclesSummary.getIssuingCompany(), vehiclesSummary.getVehiclesNumber());
            } else {
                Integer actualValue = issuingCompanyGraphicDataBean.getIssuingCompanies().get(vehiclesSummary.getIssuingCompany());
                issuingCompanyGraphicDataBean.getIssuingCompanies().put(vehiclesSummary.getIssuingCompany(), actualValue+vehiclesSummary.getVehiclesNumber());
            }
        }
        return issuingCompanyGraphicDataBean;
    }

    public CheckingCompanyGraphicDataBean getCheckingCompanyGraphicDataBean(Date startTime, Date endTime){
        CheckingCompanyGraphicDataBean checkingCompanyGraphicDataBean = new CheckingCompanyGraphicDataBean();
        List<PassengersSummary> passengers = passengersSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);
        for(PassengersSummary passengersSummary:passengers){
            if(!checkingCompanyGraphicDataBean.getCheckingCompanies().containsKey(passengersSummary.getCheckingCompany())){
                checkingCompanyGraphicDataBean.getCheckingCompanies().put(passengersSummary.getCheckingCompany(), passengersSummary.getPassengersNumber());
            } else {
                Integer actualValue = checkingCompanyGraphicDataBean.getCheckingCompanies().get(passengersSummary.getCheckingCompany());
                checkingCompanyGraphicDataBean.getCheckingCompanies().put(passengersSummary.getCheckingCompany(), actualValue+passengersSummary.getPassengersNumber());
            }
        }
        List<VehiclesSummary> vehicles = vehiclesSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThan(startTime, endTime);
        for(VehiclesSummary vehiclesSummary: vehicles){
            if(!checkingCompanyGraphicDataBean.getCheckingCompanies().containsKey(vehiclesSummary.getCheckingCompany())){
                checkingCompanyGraphicDataBean.getCheckingCompanies().put(vehiclesSummary.getCheckingCompany(), vehiclesSummary.getVehiclesNumber());
            } else {
                Integer actualValue = checkingCompanyGraphicDataBean.getCheckingCompanies().get(vehiclesSummary.getCheckingCompany());
                checkingCompanyGraphicDataBean.getCheckingCompanies().put(vehiclesSummary.getCheckingCompany(), actualValue+vehiclesSummary.getVehiclesNumber());
            }
        }
        return checkingCompanyGraphicDataBean;
    }

    public PassengersTicketsDataBean getPassengersTicketDataBean(Date startTime, Date endTime, Map<String, Boolean> issuingCompaniesSelected, Map<String, Boolean> checkingCompaniesSelected) {
        PassengersTicketsDataBean passengersTicketsDataBean = new PassengersTicketsDataBean();
        List<PassengersSummary> passengersSummaries = passengersSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);
        for (PassengersSummary passenger : passengersSummaries) {
            if(issuingCompaniesSelected.getOrDefault(passenger.getIssuingCompany(), false) &&
                    checkingCompaniesSelected.getOrDefault(passenger.getCheckingCompany(), false)){
                String type = "";
                if(passenger.getType().equals("1")){
                    type = "Adults";
                } else if(passenger.getType().equals("2")){
                    type = "Childs";
                } else if(passenger.getType().equals("3")){
                    type = "Babies";
                }
                if (!passengersTicketsDataBean.getPassengersType().containsKey(type)) {
                    passengersTicketsDataBean.getPassengersType().put(type, passenger.getPassengersNumber());
                } else {
                    Integer actualValue = passengersTicketsDataBean.getPassengersType().get(type);
                    passengersTicketsDataBean.getPassengersType().put(type, actualValue + passenger.getPassengersNumber());
                }
            }

        }
        return passengersTicketsDataBean;
    }

    public VehiclesTicketsDataBean getVehiclesTicketsDataBean(Date startTime, Date endTime, Map<String, Boolean> issuingCompaniesSelected, Map<String, Boolean> checkingCompaniesSelected){
        VehiclesTicketsDataBean vehiclesTicketsDataBean = new VehiclesTicketsDataBean();
        List<VehiclesSummary> vehiclesSummaries = vehiclesSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThan(startTime, endTime);
        for(VehiclesSummary vehicle: vehiclesSummaries){
            if (issuingCompaniesSelected.getOrDefault(vehicle.getIssuingCompany(), false) &&
                    checkingCompaniesSelected.getOrDefault(vehicle.getCheckingCompany(), false)){
                if(!vehiclesTicketsDataBean.getVehiclesType().containsKey(vehicle.getType())){
                    vehiclesTicketsDataBean.getVehiclesType().put(vehicle.getType(), vehicle.getVehiclesNumber());
                } else {
                    Integer actualValue = vehiclesTicketsDataBean.getVehiclesType().get(vehicle.getType());
                    vehiclesTicketsDataBean.getVehiclesType().put(vehicle.getType(), actualValue + vehicle.getVehiclesNumber());
                }
            }

        }
        return vehiclesTicketsDataBean;
    }

    public Map<String, Integer> getCompaniesTable(Date startTime, Date endTime){
        Map<String, Integer> result = new HashMap<>();
        List<PassengersSummary> passengersSummaries = passengersSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);
        for(PassengersSummary passenger: passengersSummaries){
            if(!result.containsKey(passenger.getCheckingCompany() + "-" + passenger.getIssuingCompany())){
                result.put(passenger.getCheckingCompany() + "-" + passenger.getIssuingCompany(), passenger.getPassengersNumber());
            } else {
                Integer actualValue = result.get(passenger.getCheckingCompany() + "-" + passenger.getIssuingCompany());
                result.put(passenger.getCheckingCompany() + "-" + passenger.getIssuingCompany(), actualValue+passenger.getPassengersNumber());
            }
        }
        List<VehiclesSummary> vehiclesSummaries = vehiclesSummaryRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThan(startTime, endTime);
        for(VehiclesSummary vehicles: vehiclesSummaries){
            if(!result.containsKey(vehicles.getCheckingCompany() + "-" + vehicles.getIssuingCompany())){
                result.put(vehicles.getCheckingCompany() + "-" + vehicles.getIssuingCompany(), vehicles.getVehiclesNumber());
            } else {
                Integer actualValue = result.get(vehicles.getCheckingCompany() + "-" + vehicles.getIssuingCompany());
                result.put(vehicles.getCheckingCompany() + "-" + vehicles.getIssuingCompany(), actualValue+vehicles.getVehiclesNumber());
            }
        }
        return result;
    }

    public TotalShipmentsDataBean getTotalShipments(Date startTime, Date endTime, Map<String, Boolean> companyFilter) {
        // Objeto donde se devuelve la información de los embarques
        TotalShipmentsDataBean totalShipmentsDataBean = new TotalShipmentsDataBean();

        // Se obtienen los datos del repositorio para las fechas seleccionadas
        List<BoardingDataSummary> boardingsData =
                boardingDataRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);

        // Se filtran los de las compañías seleccionadas y se cuentan
        boardingsData.stream()
                .filter(boardingDataSummary -> companyFilter.getOrDefault(boardingDataSummary.getCompany(), false))
                .forEach(boardingDataSummary -> countShipment(boardingDataSummary, totalShipmentsDataBean));

        return totalShipmentsDataBean;
    }

    private void countShipment(BoardingDataSummary boardingDataSummary, TotalShipmentsDataBean totalShipmentsDataBean)
    {
        if (boardingDataSummary.getStatus().equalsIgnoreCase("CERRADO"))
        {
            totalShipmentsDataBean.setClosedNumber(totalShipmentsDataBean.getClosedNumber()+1);
        }
        else if (boardingDataSummary.getStatus().equalsIgnoreCase("ABIERTO"))
        {
            totalShipmentsDataBean.setClosedNumber(totalShipmentsDataBean.getOpenedNumber()+1);
        }
        else if (boardingDataSummary.getStatus().equalsIgnoreCase("NO_INFORMADO"))
        {
            totalShipmentsDataBean.setNotReportNumber(totalShipmentsDataBean.getNotReportNumber()+1);
        }

        totalShipmentsDataBean.setNewTickets(totalShipmentsDataBean.getNewTickets() + boardingDataSummary.getNewTickets());
        totalShipmentsDataBean.setDownTickets(totalShipmentsDataBean.getDownTickets() + boardingDataSummary.getDownTickets());
        totalShipmentsDataBean.setPendingTickets(totalShipmentsDataBean.getPendingTickets() + boardingDataSummary.getPendingTickets());
    }

    public List<Boarding> getBoardings(Date startTime, Date endTime, Map<String, Boolean> companyFilter) {
        // Se obtienen los datos del repositorio para las fechas seleccionadas
        List<BoardingDataSummary> boardingsData =
                boardingDataRepository.findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(startTime, endTime);

        // Se filtran los de las compañías seleccionadas y se crean los dto
        List<Boarding> boardingsDTO = new ArrayList<>();
        boardingsData.stream()
                .filter(boardingDataSummary -> companyFilter.getOrDefault(boardingDataSummary.getCompany(), false))
                .forEach(boardingDataSummary -> boardingsDTO.add(new Boarding(boardingDataSummary)));

        return boardingsDTO;
    }
}