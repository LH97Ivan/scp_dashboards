package es.apba.oper.pcs.scp.dashboards.Data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

@Data
@Entity
public class SalesData implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    String company;

    LocalDateTime departureDate;
    String arrivalPort;
    Integer totalPassengers;
    Integer totalBus;
    Integer totalOtherVehicles;

    Instant requestedTime;

}
