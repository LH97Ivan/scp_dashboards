package es.apba.oper.pcs.scp.dashboards.Views;

import es.apba.oper.pcs.scp.dashboards.Data.UserData;
import es.apba.oper.pcs.scp.dashboards.Services.StatisticsSummaryService;
import es.apba.oper.pcs.scp.dashboards.Beans.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;


@Data
@ManagedBean
@ViewScoped
public class GeneralDashboardViewBean implements Serializable {

    private int totalVehiclesAndPassengers;
    private Date startTime = Date.from(Instant.now().minus(366, ChronoUnit.DAYS));
    private Date endTime = Date.from(Instant.now());
    private IssuingCompanyGraphicDataBean issuingCompanyGraphicDataBean;
    private CheckingCompanyGraphicDataBean checkingCompanyGraphicDataBean;
    private TotalTicketsDataBean totalTicketsDataBean;
    private PassengersTicketsDataBean passengersTicketsDataBean;
    private VehiclesTicketsDataBean vehiclesTicketsDataBean;

    private Map<String, Boolean> issuingCompaniesSelected = new HashMap<>();
    private Map<String, Boolean> checkingCompaniesSelected = new HashMap<>();
    private boolean allIssuingFilters;
    private boolean allCheckingFilters;

    private Map<String, Integer> companiesTable = new HashMap<>();

    private Map<String, Boolean> companyFilter = new HashMap<>();
    private TotalShipmentsDataBean totalBoardings;
    private boolean allCompanyFilters;

    private List<Boarding> boardings = new ArrayList<>();

    private final List<String> totalCompanies = Arrays.asList("AML", "BALEARIA", "FRS", "INTERSHIPPING", "TRASMEDITERRANEA", "ADDOCEAN_1", "ADDOCEAN_2", "ADDOCEAN_3");

    private Boolean disableFilters;

    @Autowired
    StatisticsSummaryService statisticsSummaryService;

    @Autowired
    private LoginBean loginBean;

    @PostConstruct
    public void init(){
        issuingCompaniesSelected.clear();
        checkingCompaniesSelected.clear();
        allIssuingFilters = false;
        allCheckingFilters = false;
        initiateFilters(loginBean.getUser());
        allCompanyFilters = false;

        onTotal();
    }

    public void initiateFilters(UserData user){
        totalCompanies.forEach(company -> issuingCompaniesSelected.put(company, false));
        totalCompanies.forEach(company -> checkingCompaniesSelected.put(company, false));
        totalCompanies.forEach(company -> companyFilter.put(company, false));
        disableFilters = true;
        if(user.isAdmin()){
            totalCompanies.forEach(company -> issuingCompaniesSelected.put(company, true));
            totalCompanies.forEach(company -> checkingCompaniesSelected.put(company, true));
            totalCompanies.forEach(company -> companyFilter.put(company, true));
            disableFilters = false;
        } else {
            issuingCompaniesSelected.put(user.getCompanyName().toUpperCase(), true);
            checkingCompaniesSelected.put(user.getCompanyName().toUpperCase(), true);
            companyFilter.put(user.getCompanyName().toUpperCase(), true);
        }
    }


    public void onTotal(){
        if(startTime!=null && endTime!=null && startTime.before(endTime)){
            setTotalVehiclesAndPassengers(statisticsSummaryService.getTotalVehiclesAndPassengers(startTime, endTime));
            setTotalTicketsDataBean(statisticsSummaryService.getTotalTicketsDataBean(startTime, endTime, issuingCompaniesSelected, checkingCompaniesSelected));
            setIssuingCompanyGraphicDataBean(statisticsSummaryService.getIssuingCompanyGraphicDataBean(startTime, endTime));
            setCheckingCompanyGraphicDataBean(statisticsSummaryService.getCheckingCompanyGraphicDataBean(startTime, endTime));
            setPassengersTicketsDataBean(statisticsSummaryService.getPassengersTicketDataBean(startTime, endTime, issuingCompaniesSelected, checkingCompaniesSelected));
            setVehiclesTicketsDataBean(statisticsSummaryService.getVehiclesTicketsDataBean(startTime, endTime, issuingCompaniesSelected, checkingCompaniesSelected));
            setCompaniesTable(statisticsSummaryService.getCompaniesTable(startTime, endTime));
            setTotalBoardings(statisticsSummaryService.getTotalShipments(startTime, endTime, companyFilter));
            setBoardings(statisticsSummaryService.getBoardings(startTime, endTime, companyFilter));
        } else{
            setTotalVehiclesAndPassengers(0);
            TotalTicketsDataBean totalTickets = new TotalTicketsDataBean();
            IssuingCompanyGraphicDataBean issuingCompanyGraphic = new IssuingCompanyGraphicDataBean();
            CheckingCompanyGraphicDataBean checkingCompanyGraphic = new CheckingCompanyGraphicDataBean();
            PassengersTicketsDataBean passengersTickets = new PassengersTicketsDataBean();
            VehiclesTicketsDataBean vehiclesTickets = new VehiclesTicketsDataBean();
            setTotalTicketsDataBean(totalTickets);
            setIssuingCompanyGraphicDataBean(issuingCompanyGraphic);
            setCheckingCompanyGraphicDataBean(checkingCompanyGraphic);
            setPassengersTicketsDataBean(passengersTickets);
            setVehiclesTicketsDataBean(vehiclesTickets);
            setCompaniesTable(new HashMap<>());
            setTotalBoardings(new TotalShipmentsDataBean());
            setBoardings(new ArrayList<>());
        }
    }

    public void filtering(){
        if(totalVehiclesAndPassengers != 0){
            setTotalTicketsDataBean(statisticsSummaryService.getTotalTicketsDataBean(startTime, endTime, issuingCompaniesSelected, checkingCompaniesSelected));
            setPassengersTicketsDataBean(statisticsSummaryService.getPassengersTicketDataBean(startTime, endTime, issuingCompaniesSelected, checkingCompaniesSelected));
            setVehiclesTicketsDataBean(statisticsSummaryService.getVehiclesTicketsDataBean(startTime, endTime, issuingCompaniesSelected, checkingCompaniesSelected));
        }
    }

    public void activateIssuingFilters(){
        if(allIssuingFilters){
            totalCompanies.forEach(company -> issuingCompaniesSelected.put(company, true));
        } else {
            totalCompanies.forEach(company -> issuingCompaniesSelected.put(company, false));
        }
        filtering();
    }

    public void activateCheckingFilters(){
        if(allCheckingFilters){
            totalCompanies.forEach(company -> checkingCompaniesSelected.put(company, true));
        } else {
            totalCompanies.forEach(company -> checkingCompaniesSelected.put(company, false));
        }
        filtering();
    }

    public Integer getTotalFromCompany(String company){
        Integer res = 0;
        for(String c: companiesTable.keySet()){
            if(c.contains(company)){
                res = res + companiesTable.get(c);
            }
        }
        return res;
    }

    public void tableCompanyFilter(){
        if(totalVehiclesAndPassengers!=0){
            setTotalBoardings(statisticsSummaryService.getTotalShipments(startTime, endTime, companyFilter));
            setBoardings(statisticsSummaryService.getBoardings(startTime, endTime, companyFilter));
        }
    }

    public void activateCompaniesFilters(){
        if(allCompanyFilters){
            totalCompanies.forEach(company -> companyFilter.put(company, true));
        } else {
            totalCompanies.forEach(company -> companyFilter.put(company, false));
        }
        tableCompanyFilter();
    }
}
