package es.apba.oper.pcs.scp.dashboards.Data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class BoardingDataSummary
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String company;
    private String vesselName;
    private Date departureTime;

    private Integer passengersNumber;
    private Integer vehiclesNumber;
    private String status;
    private Integer newTickets;
    private Integer downTickets;
    private Integer pendingTickets;
}
