package es.apba.oper.pcs.scp.dashboards.Data.rest;

import lombok.Data;

@Data
public class SalesREST
{
    String departureDate;
    String departureTime;
    String arrivalPort;
    Integer totalPassengers;
    Integer totalBus;
    Integer totalOtherVehicles;
}
