package es.apba.oper.pcs.scp.dashboards.Beans;

import es.apba.oper.pcs.scp.dashboards.Data.SalesData;
import lombok.Data;
import java.time.format.DateTimeFormatter;

@Data
public class SalesBoardingDTO
{
    String company;
    String departureTime;
    Integer passengers;
    Integer vehicles;
    Integer buses;
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM HH:mm");

    public SalesBoardingDTO(String company, SalesData salesData)
    {
        this.company = company + " (" + salesData.getArrivalPort() + ")";
        departureTime = salesData.getDepartureDate().format(dateTimeFormatter);
        passengers = salesData.getTotalPassengers();
        vehicles = salesData.getTotalOtherVehicles();
        buses = salesData.getTotalBus();
    }
}
