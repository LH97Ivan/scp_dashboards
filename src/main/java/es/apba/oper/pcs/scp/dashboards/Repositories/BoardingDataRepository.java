package es.apba.oper.pcs.scp.dashboards.Repositories;

import es.apba.oper.pcs.scp.dashboards.Data.BoardingDataSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface BoardingDataRepository extends JpaRepository<BoardingDataSummary, Long>
{
    List<BoardingDataSummary> findByDepartureTimeGreaterThanAndDepartureTimeLessThanOrderByDepartureTimeDesc(Date startTime, Date endTime);
}
