package es.apba.oper.pcs.scp.dashboards.Beans;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

@Data
public class CheckingCompanyGraphicDataBean implements Serializable {

    private Map<String, Integer> checkingCompanies;

    public CheckingCompanyGraphicDataBean(){
        checkingCompanies = new TreeMap<>();
    }

    public CheckingCompanyGraphicDataBean(Map<String, Integer> checkingCompanies) {
        this.checkingCompanies = checkingCompanies;
    }
}
