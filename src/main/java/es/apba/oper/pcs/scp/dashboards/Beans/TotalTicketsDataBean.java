package es.apba.oper.pcs.scp.dashboards.Beans;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

@Data
public class TotalTicketsDataBean implements Serializable {

    private Map<String, Integer> totalTickets;

    public TotalTicketsDataBean(){
        totalTickets= new TreeMap<>();
    }

    public TotalTicketsDataBean(Map<String, Integer> totalTickets) {
        this.totalTickets = totalTickets;
    }
}
