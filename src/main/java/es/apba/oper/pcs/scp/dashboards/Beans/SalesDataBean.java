package es.apba.oper.pcs.scp.dashboards.Beans;

import lombok.Data;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class SalesDataBean
{

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private List<Integer> busesData = new ArrayList<>();
    private List<Integer> passengersData = new ArrayList<>();
    private List<Integer> vehiclesData = new ArrayList<>();
    private List<String> daysApplied = new ArrayList<>();
    private List<SalesBoardingDTO> boardings = new ArrayList<>();
    private String actualDay = Date.from(Instant.now()).toInstant().atZone(ZoneId.of("Europe/Madrid")).format(dateTimeFormatter);
    private Double threshold1 = 600.0;
    private Double threshold2 = 700.0;
}
