select count(*), vpv.vehicletype, issuingCompany.companyname as issuingCompany, checkingCompany.companyname as checkinCompany, checkingBoarding.departuredate, checkingBoarding.departuretime, vessel.vesselname, checkingBoarding.id
from vehicleprintedview vpv
	join boardingdata checkingBoarding on checkingBoarding.id = vpv.checkinboardingid
	join vesseldata vessel on checkingBoarding.vessel_id = vessel.id
	join companydata checkingCompany on checkingCompany.id = vessel.company_id
	join companydata issuingCompany on issuingCompany.companyCode = vpv.issuingCompanyCode
group by checkingCompany.companyname, issuingCompany.companyname, vpv.vehicletype, checkingBoarding.departuredate, checkingBoarding.departuretime, vessel.vesselname, checkingBoarding.id
order by checkingBoarding.id
	;


select count(*), ppv.agecategory, issuingCompany.companyname as issuingCompany, checkingCompany.companyname as checkinCompany, checkingBoarding.departuredate, checkingBoarding.departuretime, vessel.vesselname, checkingBoarding.id
from passengerprintedview ppv
	join boardingdata checkingBoarding on checkingBoarding.id = ppv.checkinboardingid
	join vesseldata vessel on checkingBoarding.vessel_id = vessel.id
	join companydata checkingCompany on checkingCompany.id = vessel.company_id
	join companydata issuingCompany on issuingCompany.companyCode = ppv.issuingCompanyCode
group by checkingCompany.companyname, issuingCompany.companyname, ppv.agecategory, checkingBoarding.departuredate, checkingBoarding.departuretime, vessel.vesselname, checkingBoarding.id
order by checkingBoarding.id
	;




