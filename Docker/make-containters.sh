#!/bin/bash

./maven-clean.sh
./maven-package.sh

rm wildfly-img/scp-dashboards-0.0.1-SNAPSHOT.war
cp ../target/scp-dashboards-0.0.1-SNAPSHOT.war wildfly-img/
docker-compose up --build
docker-compose down
docker rmi wildfly-scpdashboards
docker rmi postgres-scpdashboards
